const Calculatrice = require("../src/Calculatrice.mjs");

const calculatrice = new Calculatrice(null)

test('Calculatrice test add number', () => {
    calculatrice.addNumberToDisplay(5)

    console.log(calculatrice.getDisplayedNumber())
    expect(calculatrice.getDisplayedNumber()).toBe("5")
})