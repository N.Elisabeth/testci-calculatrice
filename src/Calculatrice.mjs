import {OPERATION} from "./Enumerations.mjs";

export default class Calculatrice {

    /**
     * @param displayDiv { HTMLParagraphElement }
     */
    constructor(displayDiv) {

        this.displayedNumber = "0"
        this.cachedNumber = "0"

        this.displayDiv = displayDiv

        if(this.displayDiv != null)
            this.displayDiv.textContent = this.displayedNumber

        this.cachedOperation = OPERATION.NONE
    }

    /**
     * @returns {HTMLParagraphElement}
     */
    getDisplayDiv(){
        return this.displayDiv
    }

    /**
     * @param number { number }
     */
    addNumberToDisplay(number){

        if(this.displayedNumber === "0"){
            this.displayedNumber = number.toString()
        }else{
            this.displayedNumber += number.toString()
        }

        if(this.displayDiv != null)
            this.displayDiv.textContent = this.displayedNumber
    }

    getDisplayedNumber(){
        return this.displayedNumber
    }

    setOperation(operationName){
        console.log("Allo ?")
        this.cachedOperation = operationName
        this.cachedNumber = this.displayedNumber
        this.displayedNumber = "0"

        if(this.displayDiv != null)
            this.displayDiv.textContent = this.displayedNumber
    }

    executeOperation(){
        switch (this.cachedOperation){
            case OPERATION.ADDITION:
                this.displayedNumber = this.add(parseInt(this.cachedNumber), parseInt(this.displayedNumber)).toString()
                break

            case OPERATION.SOUSTRACTION:
                this.displayedNumber = this.substract(parseInt(this.cachedNumber), parseInt(this.displayedNumber)).toString()
                break

            case OPERATION.MULTIPLICATION:
                this.displayedNumber = this.multiply(parseInt(this.cachedNumber), parseInt(this.displayedNumber)).toString()
                break

            case OPERATION.DIVISION:
                this.displayedNumber = this.divide(parseInt(this.cachedNumber), parseInt(this.displayedNumber)).toString()
                break

            default:
                console.warn(`This operation is not implemented`)
        }

        if(this.displayDiv != null)
            this.displayDiv.textContent = this.displayedNumber
    }


    add(a, b){
        return a + b
    }


    substract(a, b){
        return a - b
    }


    divide(a, b){
        return a / b
    }


    multiply(a, b){
        return a * b
    }

    reset(){
        this.displayedNumber = "0"
        this.cachedNumber = "0"
        this.cachedOperation = OPERATION.NONE

        if(this.displayDiv != null)
            this.displayDiv.textContent = this.displayedNumber
    }
}