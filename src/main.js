import Calculatrice from "./Calculatrice.mjs";
import {OPERATION, PASSED_INFORMATION} from "./Enumerations.mjs";


let displayDiv = document.getElementById("display")
let calculatrice = new Calculatrice(displayDiv)


const numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

numbers.forEach((item) => {
    const div = document.getElementById(item.toString())
    div.addEventListener('click', function (){
        handleClickFromTemplate(PASSED_INFORMATION.NUMBER, item)
    })
})

const operations = [
    OPERATION.ADDITION,
    OPERATION.SOUSTRACTION,
    OPERATION.DIVISION,
    OPERATION.MULTIPLICATION,
    OPERATION.EQUALS,
    OPERATION.ERASE,
    OPERATION.ERASEALL
]

operations.forEach((item) => {
    const div = document.getElementById(item.toString())
    div.addEventListener('click', function (){
        handleClickFromTemplate(PASSED_INFORMATION.OPERATION, item)
    })
})









































function handleClickFromTemplate(informationType, information){

    if(informationType === PASSED_INFORMATION.NUMBER){
        calculatrice.addNumberToDisplay(information)

    }

    if(informationType === PASSED_INFORMATION.OPERATION){
        console.log("information :", information)
        switch (information){
            case OPERATION.ERASE:
                calculatrice.reset()
                break

            case OPERATION.ADDITION:
                calculatrice.setOperation(information)
                break

            case OPERATION.SOUSTRACTION:
                calculatrice.setOperation(information)
                break

            case OPERATION.MULTIPLICATION:
                calculatrice.setOperation(information)
                break

            case OPERATION.DIVISION:
                calculatrice.setOperation(information)
                break

            case OPERATION.EQUALS:
                calculatrice.executeOperation()
                break

            default:
                console.warn(`This operation (${information}) has not been implemented yet`)
                break
        }
    }



}


